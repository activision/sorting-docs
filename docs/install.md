# Installation
## jupyterhub
Detailed installation of jutyterhub
### Conda

## slurm
How to setup slurm.

## Sorting and other tools
Install nerf-sorting tools.

### Install SPYKING-CIRCUS

- Create an environment: 
  * `conda create -n sort_sc python=2.7 pip numpy matplotlib scipy  pyqt
  ipython-notebook requests six mpi4py --yes`
  * `source activate sort_sc`

- Spyking-circus needs mpi4py and can use cudamat. If you want to use it in a
cluster, across multiple computers, make sure that you compile it versus the
correct MPI library. Otherwise just do:
   * `conda install mpi4py`
   Download cudamat and install it as well (This assumes you already installed
   the cuda drivers...)
   * git clone https://github.com/cudamat/cudamat.git
   * cd cudamat && python setup.py install
- If you install parallel h5py, storage space is smaller and some parts of the
code are faster. However you have to compile it by hand... (The alternative is
doing `conda install h5py`)
   * `curl http://www.hdfgroup.org/ftp/HDF5/current/src/hdf5-1.8.16.tar | tar
   xv`
   * `INSTALLDIR=`conda env list | grep $CONDA_DEFAULT_ENV | awk '{ print $3 }'``
   * `cd hdf5-1.8.16/ && ./configure --prefix=$INSTALLDIR --enable-parallel
   --enable-shared`
   * `make && make install` which will take a while...
   * `python setup.py configure --mpi --hdf5=$INSTALLDIR`
   * `python setup.py install`
- Download spyking-circus from bitbucket (talk to Pierre Yger to get a link).
   * python setup.py install

### jupyterhub
### Configure and launch

## Rebooting
Uppon rebooting the machine you might have to get some thing done.
### Launch the nvidia kernel modules:
    `sudo modprobe nvidia`
    `ls -la /dev/nvidia*`

### Start the jupyterhub:
    `sudo su`
    `screen -R`
    `source activate sorting`
    `cd \root`
    `jupyterhub --config jupyterhub_config.py --no-ssl`

### Mount nerffs01 
    `mountnerffs_boninlab`
    `mountnerffs_retinalab`

