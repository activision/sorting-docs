# NERF Spike sorting toolbox documentation

Tools for loading data files and pre-process multichannel electrophysiology data.

These tools aim is to aid spike sorting of high-density electrophysiological probes by providing fast submittion of spike sorting jobs as well as basic tools for data visualization.

The code repository is available at: https://bitbucket.org/activision/sorting.git
It provides a simple interface via jupyterhub python notebooks to data stored at NERFFS01 and to job submission using a local computing cluster queue.

NERF - Neuro-Electronics Research Flanders
http://www.nerf.be/
