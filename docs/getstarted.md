# Getting started

If you are lucky enought that someone set everything up for you, just open an
 internet browser and point it to: [mcnanalysis01:8000](http://mcnanalysis01:8000).

This will direct you to a [jupyterhub
notebook](https://github.com/jupyter/jupyterhub) from where you can launch and
monitor jobs.

## Spike sorting @ the Farrowlab

### Job submittion
- Login as:
      + user: farrowlab
      + password: <you know it>

- Navigate to the `notebooks` folder.
- Duplicate the `sorting_template_file.ipynb` (a duplicate button shows up when you select the file.)
- Enter the duplicate notebook and rename to something that makes sense.
- On the first cell, replace `expname` with the experiment you want to analyse and run that cell.
- When that cell finishes copying and converting data run the second cell.
- Click `Sort selected files` to submit the spike sorting job.

### Job monitoring
After logging in as farrowlab open a terminal window. 
- You can monitor the job queue by doing `squeue`
- You can monitor spike sorting job progress by navigating to the job
directory and looking at the job log file:
`tail -f /scratch/retinalab/data/sort/00316/log_56.out`
just replace `00316` by the experiment name and `56` by the job name. You can
findout the job name by looking at the output of the cell you used to submit
the job. `ctr-c` exits the `tail` command.

### Manual curation of the results.
Talk to joao about this.
